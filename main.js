//1 варіант:
// let input = prompt("Введіть число");
// if(input < 5 || input === "" || isNaN(input) || input === null )
// {
//     console.log("Sorry, no numbers")
// } else {
// for (let num = 0; num <= input; num += 5) {
//   console.log(num);
// }
// }

//2 варіант:
let input = prompt("Введіть число");
if (input < 5 || input === "" || isNaN(input) || input === null) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 0; i <= input; i++) {
    if (i % 5 == 0) {
      console.log(i);
    }
  }
}
